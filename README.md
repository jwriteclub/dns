# DNS

[![Run Status](https://api.shippable.com/projects/5748714c2a8192902e21a2f1/badge?branch=master)](https://app.shippable.com/projects/5748714c2a8192902e21a2f1) [![Coverage Badge](https://api.shippable.com/projects/5748714c2a8192902e21a2f1/coverageBadge?branch=master)](https://app.shippable.com/projects/5748714c2a8192902e21a2f1) 

DNS implementation in pure PHP. Also includes support for ".bit" domain names.

## Getting started

`composer update`

`./vendor/bin/phpunit`

`php ./example/query.php google.com`
`php ./example/query.php wikileaks.bit`

## Copyright

The DNS library is licensed under the [MIT License](./LICENSE.md).
