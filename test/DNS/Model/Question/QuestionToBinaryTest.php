<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\Question;

use DNS\Model\DomainName;
use DNS\Model\QueryClass;
use DNS\Model\QueryType;
use DNS\Model\Question;

/**
 * @covers \DNS\Model\Question::toBinary
 */
class QuestionToBinaryTest extends \PHPUnit_Framework_TestCase {

    const DEFAULT_NAME_BINARY = "\x03abc\x03com\x00";

    /** @var Question */
    protected $q;

    public function setUp() {
        parent::setUp();
        $this->q = new Question(DomainName::fromString("abc.com."));
    }

    public function testBasicQuestion() {
        $this->assertEquals(self::DEFAULT_NAME_BINARY."\x00\x00"."\x00\x00", $this->q->toBinary(), "Got back expected question");
    }

    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Empty name
     */
    public function testNullQuestion() {
        $q = new Question();
        $q->toBinary();
    }

    public function testQuestionType() {
        $this->q->type = QueryType::TYPE_A;
        $this->assertEquals(self::DEFAULT_NAME_BINARY."\x00\x01"."\x00\x00", $this->q->toBinary());
    }
    public function testQuestionTypeMaximum() {
        $this->q->type = 65535;
        $this->assertEquals(self::DEFAULT_NAME_BINARY."\xff\xff"."\x00\x00", $this->q->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Type out of bounds
     */
    public function testQuestionTypeOutOfBounds() {
        $this->q->type = 65536;
        $this->q->toBinary();
    }

    public function testQuestionClass() {
        $this->q->class = QueryClass::CLASS_IN;
        $this->assertEquals(self::DEFAULT_NAME_BINARY."\x00\x00"."\x00\x01", $this->q->toBinary());
    }
    public function testQuestionClassMaximum() {
        $this->q->class = 65535;
        $this->assertEquals(self::DEFAULT_NAME_BINARY."\x00\x00"."\xff\xff", $this->q->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Class out of bounds
     */
    public function testQuestionClassOutOfBounds() {
        $this->q->class = 65536;
        $this->q->toBinary();
    }
}