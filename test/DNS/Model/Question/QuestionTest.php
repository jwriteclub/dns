<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\Question;

use DNS\Model\DomainName;
use DNS\Model\Question;

class QuestionTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers \DNS\Model\Question::__construct()
     */
    public function testNullQuestion() {
        $q = new Question();
        $this->assertNull($q->name);
    }

    /**
     * @covers \DNS\Model\Question::__construct()
     */
    public function testStringQuestion() {
        $q = new Question("abc.com");
        $this->assertNotNull($q->name);
        $this->assertTrue($q->name instanceof DomainName);
        $this->assertEquals("abc.com.", $q->name->toString());
    }

    /**
     * @covers \DNS\Model\Question::__construct()
     */
    public function testDomainNameQuestion() {
        $q = new Question(DomainName::fromString("abc.com"));
        $this->assertNotNull($q->name);
        $this->assertTrue($q->name instanceof DomainName);
        $this->assertEquals("abc.com.", $q->name->toString());
    }

}