<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\DomainName;

use DNS\Model\DomainName;

/**
 * @covers \DNS\Model\DomainName::toString()
 * @covers \DNS\Model\DomainName::__toString()
 */
class DomainNameToStringTest extends \PHPUnit_Framework_TestCase {

    public function testToStringWithRoot() {
        $d = DomainName::fromString("a.b.c.d");

        $this->assertNotNull($d, "Got a valid domain name");

        $this->assertEquals("a.b.c.d.", $d->toString(), "String includes trailling dot");
    }

    public function testToStringWithoutRoot() {
        $d = DomainName::fromString("a.b.c.d.");

        $this->assertNotNull($d, "Got a valid domain name");

        $this->assertEquals("a.b.c.d", $d->toString(false), "String excludes trailling dot");

        $this->assertEquals("a.b.c.d", $d, "Auto _toString excludes trailing dot");
    }

}