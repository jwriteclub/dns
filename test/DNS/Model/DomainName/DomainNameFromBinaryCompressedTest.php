<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\DomainName;

use DNS\Model\DataReader;
use DNS\Model\DomainName;

/**
 * @covers DNS\Model\DomainName::fromBinary()
 */
class DomainNameFromBinaryCompressedTest extends \PHPUnit_Framework_TestCase {

    public function testBasicCompressedLabel() {
        $dr = new DataReader("\x03com\x00\x03abc\xc0\x00");

        $dr->next(5);

        $d = DomainName::fromBinary($dr);

        $this->assertNotNull($d);
        $this->assertEquals("abc.com.", $d->toString());
    }

    public function testForwardCompressedLabel() {
        $dr = new DataReader("\xc0\x02\x03abc\x03com\x00");

        $d1 = DomainName::fromBinary($dr);

        $this->assertNotNull($d1);
        $this->assertEquals("abc.com.", $d1->toString());

        $d2 = DomainName::fromBinary($dr);

        $this->assertNotNull($d2);
        $this->assertEquals("abc.com.", $d2->toString());
    }

    public function testMultipleCompressedLabels() {
        $dr = new DataReader("header\x01a\xc0\x0e\x01b\xc0\x06\x01c\x03com\x00footer");

        $dr->next(6); // Strip off header data

        $d1 = DomainName::fromBinary($dr);
        $this->assertNotNull($d1);
        $this->assertEquals("a.c.com.", $d1->toString());

        $d2 = DomainName::fromBinary($dr);
        $this->assertNotNull($d2);
        $this->assertEquals("b.a.c.com.", $d2->toString());

        $d3 = DomainName::fromBinary($dr);
        $this->assertNotNull($d3);
        $this->assertEquals("c.com.", $d3->toString());

        $this->assertEquals("footer", $dr->next(6));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Invalid label character
     */
    public function testLabelPointerOffByOne() {
        $dr = new DataReader("header\x01a\xc0\x0d\x01b\xc0\x06\x01c\x03com\x00footer");

        $dr->next(6); // Strip off header data

        DomainName::fromBinary($dr);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Nefarious compression
     */
    public function testCompressionLoop() {
        $dr = new DataReader("\x01a\xc0\x00");

        DomainName::fromBinary($dr);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Nefarious compression
     */
    public function testComplexCompressionLoop() {
        $dr = new DataReader("\x01a\xc0\x06\x01b\x01c\xc0\x00\x01d\x00");

        DomainName::fromBinary($dr);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Incomplete compression
     */
    public function testIncompleteCompression() {
        $dr = new DataReader("\x01a\xc0");

        DomainName::fromBinary($dr);
    }

    // The next three strings are from http://www.securiteam.com/exploits/2CVQ4QAQNM.html

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Insufficient data
     */
    public function test990530_1() {
        $dr = new DataReader("\xc0\x0c\xc0\x07\xc0\x10\xc0\x17\xc0\x20\xc0\x27\xc0\x30\xc0\xff\xcf\x00\x00\x00\x01\x00\x01");

        DomainName::fromBinary($dr);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Insufficient data
     */
    public function test990530_2() {
        $dr = new DataReader("\xc0\x0e\xc0\x0c\xc0\x10\xc0\x17\xc0\x20\xc0\x27\xc0\x30\xc0\xff\xcf\x00\x00\x00\x01\x00\x01");

        DomainName::fromBinary($dr);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Input string label is too long
     */
    public function test990530_3() {
        $dr = new DataReader("\x3ethisleetostringwillcrashyourlittlenameserverforsurehahahahahah\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\xc0\x0c\x00");

        DomainName::fromBinary($dr);
    }

}