<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\DomainName;

use DNS\Model\DataReader;
use DNS\Model\DomainName;

/**
 * @covers \DNS\Model\DomainName::fromBinary()
 */
class DomainNameFromBinaryTest extends \PHPUnit_Framework_TestCase {

    public function testBasicBinaryName() {
        $d = DomainName::fromBinary(new DataReader("\x02ab\x03com\x00"));

        $this->assertNotNull($d, "Got back a domain name");

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(3, count($labels), "Got back 3 labels");
        $this->assertEquals(3, count($lengths), "Got back 3 lengths");

        $this->assertEquals("ab", $labels[0], "Got back correct label 0");
        $this->assertEquals(2, $lengths[0], "Got back correct length 0");

        $this->assertEquals("com", $labels[1], "Got back correct label 1");
        $this->assertEquals(3, $lengths[1], "Got back correct length 1");

        $this->assertEquals("", $labels[2], "Got back correct label 2");
        $this->assertEquals(0, $lengths[2], "Got back correct length 2");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Invalid label length overrun
     */
    public function testCounterTriesToOverrun() {
        DomainName::fromBinary(new DataReader("\x02ab\x05com\x00"));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Input string is empty
     */
    public function testEmptyString() {
        DomainName::fromBinary(new DataReader(""));
    }

    public function testMaximumLengthName() {
        $name = chr(61).str_repeat('a', 61).chr(63).str_repeat('b', 63).chr(63).str_repeat('c', 63).chr(63).str_repeat('d', 63).chr(0);
        $this->assertEquals(255, strlen($name), "Maximum name is 255 bytes");

        $d = DomainName::fromBinary(new DataReader($name));

        $this->assertNotNull($d);

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(5, count($labels), "Got back 5 labels");
        $this->assertEquals(5, count($lengths), "Got back 5 lengths");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Input string is longer than the maximum allowed length
     */
    public function testOverLongName() {
        $name = chr(62).str_repeat('a', 62).chr(63).str_repeat('b', 63).chr(63).str_repeat('c', 63).chr(63).str_repeat('d', 63).chr(0);
        $this->assertEquals(256, strlen($name), "Maximum name is 255 bytes");
        DomainName::fromBinary(new DataReader($name));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Input string label is too long
     */
    public function testOverLongLabel() {
        $name = chr(60).str_repeat('a', 60).chr(64).str_repeat('b', 64).chr(63).str_repeat('c', 63).chr(63).str_repeat('d', 63).chr(0);
        $this->assertEquals(255, strlen($name), "Maximum name is 255 bytes");
        DomainName::fromBinary(new DataReader($name));
    }

    protected function extractLabelsAndLengths(DomainName $d) {
        $class = new \ReflectionClass(DomainName::class);
        $labelsProperty = $class->getProperty("labels");
        $labelsProperty->setAccessible(true);
        $labelLengthsProperty = $class->getProperty("labelLengths");
        $labelLengthsProperty->setAccessible(true);

        return array($labelsProperty->getValue($d), $labelLengthsProperty->getValue($d));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Invalid label character
     */
    public function testInvalidLabelCharacter() {
        DomainName::fromBinary(new DataReader("\x02ab\x03c_m\x00"));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label begins with invalid character -
     */
    public function testInvalidFirstLabelCharacter() {
        DomainName::fromBinary(new DataReader("\x02ab\x03-om\x00"));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label ends with invalid character -
     */
    public function testInvalidLastLabelCharacter() {
        DomainName::fromBinary(new DataReader("\x02a-\x03com\x00"));
    }
}