<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\DomainName;

use DNS\Model\DomainName;
use DNS\Model\InvalidNameException;

/**
 * @covers \DNS\Model\DomainName::fromString()
 * @covers \DNS\Model\DomainName::addLabel()
 */
class DomainNameFromStringTest extends \PHPUnit_Framework_TestCase {

    public function testSimpleName() {
        $d = DomainName::fromString("example.com");

        $this->assertNotNull($d, "Got back a valid domain name");

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(3, count($labels), "Labels has 3 entries");
        $this->assertEquals(3, count($lengths), "Lengths has 3 entries");

        $this->assertEquals("example", $labels[0], "Correct domain name label");
        $this->assertEquals(7, $lengths[0], "Correct domain name length");

        $this->assertEquals("com", $labels[1], "Correct tld name label");
        $this->assertEquals(3, $lengths[1], "Correct tld name length");

        $this->assertEquals("", $labels[2], "Correct root name label");
        $this->assertEquals(0, $lengths[2], "Correct root name length");
    }

    public function testSimpleNameWithDot() {
        $d = DomainName::fromString("example.com.");

        $this->assertNotNull($d, "Got back a valid domain name");

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(3, count($labels), "Labels has 3 entries");
        $this->assertEquals(3, count($lengths), "Lengths has 3 entries");

        $this->assertEquals("example", $labels[0], "Correct domain name label");
        $this->assertEquals(7, $lengths[0], "Correct domain name length");

        $this->assertEquals("com", $labels[1], "Correct tld name label");
        $this->assertEquals(3, $lengths[1], "Correct tld name length");

        $this->assertEquals("", $labels[2], "Correct root name label");
        $this->assertEquals(0, $lengths[2], "Correct root name length");
    }

    public function testTLDOnlyName() {
        // This name is degenerate from a usefulness perspective, but perfectly valid otherwise
        $d = DomainName::fromString("com");

        $this->assertNotNull($d, "Got back a valid domain name");

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(2, count($labels), "Labels has 2 entries");
        $this->assertEquals(2, count($lengths), "Lengths has 2 entries");

        $this->assertEquals("com", $labels[0], "Correct tld name label");
        $this->assertEquals(3, $lengths[0], "Correct tld name length");

        $this->assertEquals("", $labels[1], "Correct root name label");
        $this->assertEquals(0, $lengths[1], "Correct root name length");
    }

    public function testMaximumLengthLabel() {
        $d = DomainName::fromString(str_repeat('a', 63).".");

        $this->assertNotNull($d);

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(2, count($labels), "Labels has 2 entries");
        $this->assertEquals(2, count($lengths), "Lengths has 2 entries");

        $this->assertEquals(str_repeat('a', 63), $labels[0], "Correct tld name label");
        $this->assertEquals(63, $lengths[0], "Correct tld name length");

        $this->assertEquals("", $labels[1], "Correct root name label");
        $this->assertEquals(0, $lengths[1], "Correct root name length");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label length 64 is greater than the maximum permitted label 63
     */
    public function testOverLengthLabel() {
        DomainName::fromString(str_repeat('a', 64).".");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label is empty
     */
    public function testEmptyLengthLabel() {
        DomainName::fromString("..");
    }

    public function testMaximumLengthName() {
        $name = str_repeat('a', 61).'.'.str_repeat('b', 63).'.'.str_repeat('c', 63).'.'.str_repeat('d', 63).'.';
        $this->assertEquals(254, strlen($name), "8 bit length input");

        $d = DomainName::fromString($name);

        $this->assertNotNull($d);

        list($labels, $lengths) = $this->extractLabelsAndLengths($d);

        $this->assertEquals(5, count($labels), "Labels has 5 entries");
        $this->assertEquals(5, count($lengths), "Lengths has 5 entries");

        $this->assertEquals(255, strlen($d->toBinary()), "Binary representation is maximum length");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Name length 253 is greater than the maximum permitted name 252
     */
    public function testOverLongNameWithDot() {
        $name = str_repeat('a', 62).'.'.str_repeat('b', 63).'.'.str_repeat('c', 63).'.'.str_repeat('d', 63).'.';
        $this->assertEquals(255, strlen($name), "8 bit length input");

        DomainName::fromString($name);
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Name length 253 is greater than the maximum permitted name 252
     */
    public function testOverLongNameWithoutDot() {
        $name = str_repeat('a', 62).'.'.str_repeat('b', 63).'.'.str_repeat('c', 63).'.'.str_repeat('d', 63);
        $this->assertEquals(255 - 1, strlen($name), "8 bit length input");

        DomainName::fromString($name);
    }

    public function testEmptyNameWithDot() {
        $name = DomainName::fromString(".");
        $this->assertEquals("", $name->toString(false));
        $this->assertEquals(".", $name->toString(true));
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Name is empty
     */
    public function testEmptyNameWithoutDot() {
        DomainName::fromString("");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Character code 95 is not a valid name character
     */
    public function testInvalidCharacterInName() {
        DomainName::fromString("hello_world.com");
    }

    public function testAllInvalidCharacters() {
        for ($i = 0; $i <= 127; $i += 1) {
            try {
                $d = DomainName::fromString("a".chr($i)."z.com");
            } catch (\Exception $ex) {
                $d = $ex;
            }
            if ($i == ord('-') || $i == ord('.')) {
                $this->assertTrue($d instanceof DomainName, "Got a valid domain name for $i");
            } else if ($i >= ord('A') && $i <= ord('Z')) {
                $this->assertTrue($d instanceof DomainName, "Got a valid domain name for $i");
            } else if ($i >= ord('a') && $i <= ord('z')) {
                $this->assertTrue($d instanceof DomainName, "Got a valid domain name for $i");
            } else if ($i >= ord('0') && $i <= ord('9')) {
                $this->assertTrue($d instanceof DomainName, "Got a valid domain name for $i");
            } else {
                $this->assertTrue($d instanceof InvalidNameException, "Got an exception for $i");
                $this->assertEquals("Character code $i is not a valid name character", $d->getMessage(), "Got the correct exception");
            }
        }
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label begins with invalid character -
     */
    public function testInvalidDashFirstCharacter() {
        DomainName::fromString("-invalid.com");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label begins with invalid character -
     */
    public function testInvalidDashInteriorFirstCharacter() {
        DomainName::fromString("abc.-invalid.com");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label ends with invalid character -
     */
    public function testInvalidDashLastCharacter() {
        DomainName::fromString("invalid-.com");
    }

    /**
     * @expectedException \DNS\Model\InvalidNameException
     * @expectedExceptionMessage Label ends with invalid character -
     */
    public function testInvalidDashInteriorLastCharacter() {
        DomainName::fromString("abc.invalid-.com");
    }

    // TODO: preserves case

    protected function extractLabelsAndLengths(DomainName $d) {
        $class = new \ReflectionClass(DomainName::class);
        $labelsProperty = $class->getProperty("labels");
        $labelsProperty->setAccessible(true);
        $labelLengthsProperty = $class->getProperty("labelLengths");
        $labelLengthsProperty->setAccessible(true);

        return array($labelsProperty->getValue($d), $labelLengthsProperty->getValue($d));
    }

}