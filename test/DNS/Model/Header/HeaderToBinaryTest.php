<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\Header;

use DNS\Model\Header;

/**
 * @covers \DNS\Model\Header::toBinary
 */
class HeaderToBinaryTest extends \PHPUnit_Framework_TestCase {

    const DEFAULT_ID = "\x12\x34";

    /** @var Header */
    protected $h;

    public function setUp() {
        parent::setUp();
        $this->h = new Header();
        // Set the ID to a known value for testing
        $this->h->id = self::DEFAULT_ID;
    }

    public function testMinimalHeader() {
        $this->assertEquals(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }

    public function testSingleRecords() {
        $this->h->questionCount = 1;
        $this->h->answerCount = 1;
        $this->h->nameserverCount = 1;
        $this->h->additionalRecordCount = 1;
        $this->assertEquals(self::DEFAULT_ID."\x00\x00"."\x00\x01"."\x00\x01"."\x00\x01"."\x00\x01", $this->h->toBinary());
    }
    public function testMaxQuestions() {
        $this->h->questionCount = 65535;
        $this->h->answerCount = 65535;
        $this->h->nameserverCount = 65535;
        $this->h->additionalRecordCount = 65535;
        $this->assertEquals(self::DEFAULT_ID."\x00\x00"."\xff\xff"."\xff\xff"."\xff\xff"."\xff\xff", $this->h->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Too many questions
     */
    public function testTooManyQuestions() {
        $this->h->questionCount = 65536;
        $this->h->toBinary();
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Too many answers
     */
    public function testTooManyAnswers() {
        $this->h->answerCount = 65536;
        $this->h->toBinary();
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Too many nameservers
     */
    public function testTooManyNameservers() {
        $this->h->nameserverCount = 65536;
        $this->h->toBinary();
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Too many additional records
     */
    public function testTooManyAdditionalRecords() {
        $this->h->additionalRecordCount = 65536;
        $this->h->toBinary();
    }

    public function testResponseCode() {
        $this->h->responseCode = Header::RESPONSE_FORMAT_ERROR;
        $this->assertEquals(self::DEFAULT_ID."\x00\x01"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    public function testResponseMaximum() {
        $this->h->responseCode = 15;
        $this->assertEquals(self::DEFAULT_ID."\x00\x0f"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Response code out of bounds
     */
    public function testOutOfBoundsResponseCode() {
        $this->h->responseCode = 16;
        $this->h->toBinary();
    }

    public function testZ() {
        $this->h->Z = 1;
        $this->assertEquals(self::DEFAULT_ID."\x00\x10"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    public function testZMaximum() {
        $this->h->Z = 7;
        $this->assertEquals(self::DEFAULT_ID."\x00\x70"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Z out of bounds
     */
    public function testOutOfBoundsZ() {
        $this->h->Z = 8;
        $this->h->toBinary();
    }

    public function testOpCode() {
        $this->h->opcode = 1;
        $this->assertEquals(self::DEFAULT_ID."\x08\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    public function testOpCodeMaximum() {
        $this->h->opcode = 15;
        $this->assertEquals(self::DEFAULT_ID."\x78\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Opcode out of bounds
     */
    public function testOutOfBoundsOpCode() {
        $this->h->opcode = 16;
        $this->h->toBinary();
    }

    public function testResponseFlag() {
        $this->h->response = true;
        $this->assertEquals(self::DEFAULT_ID."\x80\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }

    public function testAuthoritativeFlag() {
        $this->h->authoritative = true;
        $this->assertEquals(self::DEFAULT_ID."\x04\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }

    public function testTruncatedFlag() {
        $this->h->truncated = true;
        $this->assertEquals(self::DEFAULT_ID."\x02\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }

    public function testRecursionDesiredFlag() {
        $this->h->recursionDesired = true;
        $this->assertEquals(self::DEFAULT_ID."\x01\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }

    public function testRecusrionAvailableFlag() {
        $this->h->recursionAvailable = true;
        $this->assertEquals(self::DEFAULT_ID."\x00\x80"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00", $this->h->toBinary());
    }
}