<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\Header;

use DNS\Model\DataReader;
use DNS\Model\Header;

/**
 * @covers \DNS\Model\Header::fromBinary()
 */
class HeaderFromBinaryTest extends \PHPUnit_Framework_TestCase {

    const DEFAULT_ID = "\x12\x34";

    public function testBasicHeader() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testResponseField() {
        $dr = new DataReader(self::DEFAULT_ID."\x80\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertTrue($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testAuthoritativeField() {
        $dr = new DataReader(self::DEFAULT_ID."\x04\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertTrue($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testTruncatedField() {
        $dr = new DataReader(self::DEFAULT_ID."\x02\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertTrue($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testRecursionDesiredField() {
        $dr = new DataReader(self::DEFAULT_ID."\x01\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertTrue($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testRecursionAvailableField() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x80"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertTrue($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testOpCode() {
        $dr = new DataReader(self::DEFAULT_ID."\x08\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(1, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testOpCodeMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x78\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(15, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testZ() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x10"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(1, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testZMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x70"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(7, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testResponseCode() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x01"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(1, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testResponseCodeMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x08"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(8, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    /**
     * @expectedException \DNS\Model\InvalidValueException
     * @expectedExceptionMessage Insufficient header data available
     */
    public function testInsufficientData() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00");
        Header::fromBinary($dr);
    }

    public function testQuestionCount() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x01"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(1, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testQuestionCountMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\xff\xff"."\x00\x00"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(65535, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testAnswerCount() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x01"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(1, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testAnswerCountMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\xff\xff"."\x00\x00"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(65535, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testNameserverCount() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x01"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(1, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }
    public function testNameserverCountMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\xff\xff"."\x00\x00");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(65535, $h->nameserverCount);
        $this->assertEquals(0, $h->additionalRecordCount);
    }

    public function testAdditionalRecordCount() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x01");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(1, $h->additionalRecordCount);
    }
    public function testAdditionalRecordCountMaximum() {
        $dr = new DataReader(self::DEFAULT_ID."\x00\x00"."\x00\x00"."\x00\x00"."\x00\x00"."\xff\xff");
        $h = Header::fromBinary($dr);

        $this->assertNotNull($h);
        $this->assertTrue($h instanceof Header);

        $this->assertEquals($h->id, self::DEFAULT_ID);
        $this->assertFalse($h->response);
        $this->assertEquals(0, $h->opcode);
        $this->assertFalse($h->authoritative);
        $this->assertFalse($h->truncated);
        $this->assertFalse($h->recursionDesired);
        $this->assertFalse($h->recursionAvailable);
        $this->assertEquals(0, $h->Z);
        $this->assertEquals(0, $h->responseCode);

        $this->assertEquals(0, $h->questionCount);
        $this->assertEquals(0, $h->answerCount);
        $this->assertEquals(0, $h->nameserverCount);
        $this->assertEquals(65535, $h->additionalRecordCount);
    }
}