<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\DataReader;

use DNS\Model\DataReader;

/**
 * @covers DNS\Model\DataReader
 */
class DataReaderTest extends \PHPUnit_Framework_TestCase {

    public function testBasicRead() {
        $dr = new DataReader("abcd");
        $testChars = array("a", "b", "c", "d");
        foreach($testChars as $char) {
            $this->assertTrue($dr->hasNext(), "Has a character available");
            $this->assertEquals($char, $dr->peek(), "Can peek the character");
            $this->assertEquals($char, $dr->next(), "Can consume the character");
        }
    }

    public function testDoubleRead() {
        $dr = new DataReader("abcdef12");
        $testChars = array("ab", "cd", "ef", "12");
        foreach($testChars as $char) {
            $this->assertTrue($dr->hasNext(2), "Has a character available");
            $this->assertEquals($char, $dr->peek(2), "Can peek the character");
            $this->assertEquals($char, $dr->next(2), "Can consume the character");
        }
    }

    public function testAt() {
        $dr = new DataReader("abcd");
        $testChars = array("a", "b", "c", "d");
        foreach($testChars as $idx => $char) {
            $this->assertEquals($char, $dr->at($idx), "Can see the character");
        }
    }

    public function testAtDouble() {
        $dr = new DataReader("abcdef12");
        $testChars = array("ab", "cd", "ef", "12");
        foreach($testChars as $idx => $char) {
            $this->assertEquals($char, $dr->at($idx*2, 2), "Can see the character");
        }
    }

    public function testEmptyRead() {
        $dr = new DataReader("abc");
        $dr->next();
        $dr->next();
        $this->assertTrue($dr->hasNext(), "Has the last item");
        $this->assertFalse($dr->hasNext(2), "Doesn't have a double read across the last item boundary");
        $dr->next();
        $this->assertFalse($dr->hasNext(), "Doesn't have an item after the last item");
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Cannot read beyond end of data
     */
    public function testPeekBeyondEnd() {
        $dr = new DataReader("abc");
        $dr->peek(4);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Cannot read beyond end of data
     */
    public function testAtBeyondEnd() {
        $dr = new DataReader("abc");
        $dr->at(2, 2);
    }

    public function testRewind() {
        $dr = new DataReader("abcd");
        $testChars = array("a", "b", "c", "d");
        foreach($testChars as $char) {
            $this->assertTrue($dr->hasNext(), "Has a character available");
            $this->assertEquals($char, $dr->peek(), "Can peek the character");
            $this->assertEquals($char, $dr->next(), "Can consume the character");
        }
        $dr->rewind();
        foreach($testChars as $char) {
            $this->assertTrue($dr->hasNext(), "Has a character available");
            $this->assertEquals($char, $dr->peek(), "Can peek the character");
            $this->assertEquals($char, $dr->next(), "Can consume the character");
        }
    }

    public function testCurrentOffset() {
        $dr = new DataReader("abcd");
        $testChars = array("a", "b", "c", "d");
        foreach($testChars as $idx => $char) {
            $this->assertTrue($dr->hasNext(), "Has a character available");
            $this->assertEquals($char, $dr->peek(), "Can peek the character");
            $this->assertEquals($idx, $dr->currentOffset(), "Has the correct offset");
            $this->assertEquals($char, $dr->next(), "Can consume the character");
            $this->assertEquals($idx + 1, $dr->currentOffset(), "Has the correct offset");
        }
    }

    public function testValidOffset() {
        $dr = new DataReader("abcd");

        $this->assertFalse($dr->validOffset(-1));

        $this->assertTrue($dr->validOffset(0));
        $this->assertTrue($dr->validOffset(1));
        $this->assertTrue($dr->validOffset(2));
        $this->assertTrue($dr->validOffset(3));

        $this->assertFalse($dr->validOffset(4));
    }
}