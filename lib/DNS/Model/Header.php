<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

class Header {

    const OPCODE_QUERY  = 0;
    const OPCODE_IQUERY = 1;
    const OPCODE_STATUS = 2;

    const RESPONSE_OK               = 0;
    const RESPONSE_FORMAT_ERROR     = 1;
    const RESPONSE_SERVER_ERROR     = 2;
    const RESPONSE_NAME_ERROR       = 3;
    const RESPONSE_NOT_IMPLEMENTED  = 4;
    const RESPONSE_REFUSED          = 5;

    const MASK_RESPONSE_CODE        = 0b0000000000001111;
    const MASK_Z                    = 0b0000000001110000;
    const MASK_RECURSION_AVAILABLE  = 0b0000000010000000;
    const MASK_RECURSION_DESIRED    = 0b0000000100000000;
    const MASK_TRUNCATED            = 0b0000001000000000;
    const MASK_AUTHORITATIVE        = 0b0000010000000000;
    const MASK_OPCODE               = 0b0111100000000000;
    const MASK_RESPONSE             = 0b1000000000000000;

    const SHIFT_RESPONSE_CODE       =  0;
    const SHIFT_Z                   =  4;
    const SHIFT_RECURSION_AVAILABLE =  7;
    const SHIFT_RECURSION_DESIRED   =  8;
    const SHIFT_TRUNCATED           =  9;
    const SHIFT_AUTHORITATIVE       = 10;
    const SHIFT_OPCODE              = 11;
    const SHIFT_RESPONSE            = 15;

    /** @var string */
    public $id;
    /** @var bool */
    public $response;
    /** @var int */
    public $opcode;
    /** @var bool */
    public $authoritative = false;
    /** @var bool */
    public $truncated = false;
    /** @var bool */
    public $recursionDesired = false;
    /** @var bool */
    public $recursionAvailable = false;
    /** @var int */
    public $Z = 0;
    /** @var int */
    public $responseCode = 0;

    /** @var int */
    public $questionCount = 0;
    /** @var int */
    public $answerCount = 0;
    /** @var int */
    public $nameserverCount = 0;
    /** @var int */
    public $additionalRecordCount = 0;

    /**
     * @codeCoverageIgnore since this breaks *some* systems otherwise
     */
    public function __construct() {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $this->id = openssl_random_pseudo_bytes(2);
        } else {
            $this->id = chr(rand(0, 255)).chr(rand(0, 255));
        }
    }

    public function toBinary() {
        $ret = $this->id;

        if ($this->questionCount > 65535) {
            throw new InvalidValueException("Too many questions");
        }
        if ($this->answerCount > 65535) {
            throw new InvalidValueException("Too many answers");
        }
        if ($this->nameserverCount > 65535) {
            throw new InvalidValueException("Too many nameservers");
        }
        if ($this->additionalRecordCount > 65535) {
            throw new InvalidValueException("Too many additional records");
        }

        if ($this->responseCode > 15) {
            throw new InvalidValueException("Response code out of bounds");
        }
        if ($this->Z > 7) {
            throw new InvalidValueException("Z out of bounds");
        }
        if ($this->opcode > 15) {
            throw new InvalidValueException("Opcode out of bounds");
        }

        $flags = 0;
        $flags |= ($this->response << self::SHIFT_RESPONSE) & self::MASK_RESPONSE;
        $flags |= ($this->opcode << self::SHIFT_OPCODE) & self::MASK_OPCODE;
        $flags |= ($this->authoritative << self::SHIFT_AUTHORITATIVE) & self::MASK_AUTHORITATIVE;
        $flags |= ($this->truncated << self::SHIFT_TRUNCATED) & self::MASK_TRUNCATED;
        $flags |= ($this->recursionDesired << self::SHIFT_RECURSION_DESIRED) & self::MASK_RECURSION_DESIRED;
        $flags |= ($this->recursionAvailable << self::SHIFT_RECURSION_AVAILABLE) & self::MASK_RECURSION_AVAILABLE;
        $flags |= ($this->Z << self::SHIFT_Z) & self::MASK_Z;
        $flags |= ($this->responseCode << self::SHIFT_RESPONSE_CODE) & self::MASK_RESPONSE_CODE;

        $ret .= pack("nnnnn", $flags, $this->questionCount, $this->answerCount, $this->nameserverCount, $this->additionalRecordCount);

        return $ret;
    }

    /**
     * @param DataReader $reader
     * @return Header
     * @throws InvalidValueException
     */
    public static function fromBinary($reader) {
        if (!$reader->hasNext(12)) {
            throw new InvalidValueException("Insufficient header data available");
        }
        $ret = new Header();
        $ret->id = $reader->next(2);

        $vals = unpack("nflags/nquestionCount/nanswerCount/nnameserverCount/nadditionalRecordCount", $reader->next(10));

        // No testing for bad values here, we must have 10 bytes (see hasNext), and we can always decode 5 uint16s from
        // 10 bytes. They may be meaningless, but we're sure to decode them.

        $flags = $vals['flags'];
        $ret->response = boolval(($flags & self::MASK_RESPONSE) >> self::SHIFT_RESPONSE);
        $ret->opcode = ($flags & self::MASK_OPCODE) >> self::SHIFT_OPCODE;
        $ret->authoritative = boolval(($flags & self::MASK_AUTHORITATIVE) >> self::SHIFT_AUTHORITATIVE);
        $ret->truncated = boolval(($flags & self::MASK_TRUNCATED) >> self::SHIFT_TRUNCATED);
        $ret->recursionDesired = boolval(($flags & self::MASK_RECURSION_DESIRED) >> self::SHIFT_RECURSION_DESIRED);
        $ret->recursionAvailable = boolval(($flags & self::MASK_RECURSION_AVAILABLE) >> self::SHIFT_RECURSION_AVAILABLE);
        $ret->Z = ($flags & self::MASK_Z) >> self::SHIFT_Z;
        $ret->responseCode = ($flags & self::MASK_RESPONSE_CODE) >> self::SHIFT_RESPONSE_CODE;

        $ret->questionCount = $vals['questionCount'];
        $ret->answerCount = $vals['answerCount'];
        $ret->nameserverCount = $vals['nameserverCount'];
        $ret->additionalRecordCount = $vals['additionalRecordCount'];

        return $ret;
    }

    ///
    /// Formatters
    ///
    public function toDig() {
        $ret = ";; ->>HEADER<<- ";

        $ret .= "opcode: ".self::digOpcodeToText($this->opcode).", ";
        $ret .= "status: ".self::digResponseCodeToText($this->responseCode).", ";
        $ret .= "id: ".unpack("nid", $this->id)['id'];
        $ret .= PHP_EOL;

        $ret .= ";; flags:";
        if ($this->response) {
            $ret .= " qr";
        }
        if ($this->authoritative) {
            $ret .= " aa";
        }
        if ($this->truncated) {
            $ret .= " tc";
        }
        if ($this->recursionDesired) {
            $ret .= " rd";
        }
        if ($this->recursionAvailable) {
            $ret .= " ra";
        }
        $ret .= ";";
        $ret .= " QUERY: ".$this->questionCount.",";
        $ret .= " ANSWER: ".$this->answerCount.",";
        $ret .= " AUTHORITY: ".$this->nameserverCount.",";
        $ret .= " ADDITIONAL: ".$this->additionalRecordCount;
        $ret .= PHP_EOL;

        return $ret;
    }

    protected static function digOpcodeToText($opcode) {
        switch($opcode) {
            case self::OPCODE_QUERY:
                return "QUERY";
            case self::OPCODE_IQUERY:
                return "IQUERY";
            case self::OPCODE_STATUS:
                return "STATUS";
            default:
                return "UNKNOWN[$opcode]";
        }
    }

    protected static function digResponseCodeToText($responseCode) {
        switch($responseCode) {
            case self::RESPONSE_OK:
                return "NOERROR";
            case self::RESPONSE_FORMAT_ERROR:
                return "FORMERR";
            case self::RESPONSE_SERVER_ERROR:
                return "SERVFAIL";
            case self::RESPONSE_NAME_ERROR:
                return "NXDOMAIN";
            case self::RESPONSE_NOT_IMPLEMENTED:
                return "NOTIMP";
            case self::RESPONSE_REFUSED:
                return "UNKNOWN";
            default:
                return "ERROR[$responseCode]";
        }
    }
}