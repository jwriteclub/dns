<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

/**
 * Class QueryType represent query types RFC 1035 3.2.3
 * @package DNS\Model
 */
class QueryType extends Type {
    const TYPE_AXFR     = 252;
    const TYPE_MAILB    = 253;
    const TYPE_MAILA    = 254;

    const TYPE_WILDCARD = 255;

    public static function digName($int) {
        switch($int) {
            case self::TYPE_AXFR:
                return "AXFR";
            case self::TYPE_MAILB:
                return "MAILB";
            case self::TYPE_MAILA:
                return "MAILA";
            case self::TYPE_WILDCARD:
                return "*";
            default:
                return parent::digName($int);
        }
    }
}