<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

/**
 * Class Type represents resource types RFC 1035 3.2.2
 * @package DNS\Model
 */
class Type {
    const TYPE_A        =  1;
    const TYPE_NS       =  2;

    const TYPE_MD       =  3;
    const TYPE_MF       =  4;

    const TYPE_CNAME    =  5;
    const TYPE_SOA      =  6;

    const TYPE_MB       =  7;
    const TYPE_MG       =  8;
    const TYPE_MR       =  9;

    const TYPE_NULL     = 10;

    const TYPE_WKS      = 11;
    const TYPE_PTR      = 12;

    const TYPE_HINFO    = 13;
    const TYPE_MINFO    = 14;

    const TYPE_MX       = 15;

    const TYPE_TXT      = 16;

    const TYPE_AAAA     = 28;

    public static function digName($int) {
        switch($int) {
            case self::TYPE_A:
                return "A";
            case self::TYPE_NS:
                return "NS";
            case self::TYPE_MD:
                return "MD";
            case self::TYPE_MF:
                return "MF";
            case self::TYPE_CNAME:
                return "CNAME";
            case self::TYPE_SOA:
                return "SOA";
            case self::TYPE_MB:
                return "MB";
            case self::TYPE_MG:
                return "MG";
            case self::TYPE_MR:
                return "MR";
            case self::TYPE_NULL:
                return "NULL";
            case self::TYPE_WKS:
                return "WKS";
            case self::TYPE_PTR:
                return "PTR";
            case self::TYPE_HINFO:
                return "HINFO";
            case self::TYPE_MINFO:
                return "MINFO";
            case self::TYPE_MX:
                return "MX";
            case self::TYPE_TXT:
                return "TXT";
            case self::TYPE_AAAA:
                return "AAAA";
            default;
                return "[ERR]";
        }
    }
}