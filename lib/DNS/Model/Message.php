<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

class Message {
    /** @var Header */
    public $header;
    /** @var Question[] */
    public $questions;
    /** @var ResourceRecord[] */
    public $answers;
    /** @var ResourceRecord[] */
    public $authorities;
    /** @var ResourceRecord[] */
    public $additionalRecords;

    public function __construct() {
        $this->header = new Header();
        $this->questions = array();
        $this->answers = array();
        $this->authorities = array();
        $this->additionalRecords = array();
    }

    /**
     * @param Question $question
     */
    public function addQuestion($question) {
        array_push($this->questions, $question);
        $this->header->questionCount += 1;
    }

    public function setRequest($recursionDesired = false) {
        $this->header->response = false;
        $this->header->recursionDesired = $recursionDesired;
        $this->header->opcode = Header::OPCODE_QUERY;
    }

    public function toBinary() {
        $ret = "";
        // TODO: Check for header
        // TODO: Check for mismatch in header counts and actual available
        $ret .= $this->header->toBinary();

        foreach($this->questions as $q) {
            $ret .= $q->toBinary();
        }

        foreach($this->answers as $ans) {
            $ret .= $ans->toBinary();
        }

        foreach($this->authorities as $a) {
            $ret .= $a->toBinary();
        }

        foreach($this->additionalRecords as $ar) {
            $ret .= $ar->toBinary();
        }

        return $ret;
    }

    /**
     * @param DataReader $reader
     * @return Message
     */
    public static function fromBinary($reader) {
        $ret = new Message();

        $ret->header = Header::fromBinary($reader);

        for($i = 0; $i < $ret->header->questionCount; $i += 1) {
            array_push($ret->questions, Question::fromBinary($reader));
        }
        for ($i = 0; $i < $ret->header->answerCount; $i += 1) {
            array_push($ret->answers, ResourceRecord::fromBinary($reader));
        }
        for ($i = 0; $i < $ret->header->nameserverCount; $i += 1) {
            array_push($ret->authorities, ResourceRecord::fromBinary($reader));
        }
        for ($i = 0; $i < $ret->header->additionalRecordCount; $i += 1) {
            array_push($ret->additionalRecords, ResourceRecord::fromBinary($reader));
        }

        return $ret;
    }

    ///
    /// Formatters
    ///
    public function toDig() {
        $ret = $this->header->toDig();
        $ret .= PHP_EOL;
        if ($this->header->questionCount > 0) {
            $ret .= ";; QUESTION SECTION:".PHP_EOL;
            for($i = 0; $i < $this->header->questionCount; $i += 1) {
                if (!isset($this->questions[$i])) {
                    throw new InvalidValueException("Missing a declared question");
                }
                $ret .= $this->questions[$i]->toDig();
            }
        }
        if ($this->header->answerCount > 0) {
            $ret .= PHP_EOL.";; ANSWER SECTION:".PHP_EOL;
            for($i = 0; $i < $this->header->answerCount; $i += 1) {
                if (!isset($this->answers[$i])) {
                    throw new InvalidValueException("Missing a declared answer");
                }
                $ret .= $this->answers[$i]->toDig();
            }
        }
        if ($this->header->nameserverCount > 0) {
            $ret .= PHP_EOL.";; AUTHORITY SECTION:".PHP_EOL;
            for($i = 0; $i < $this->header->nameserverCount; $i += 1) {
                if (!isset($this->authorities[$i])) {
                    throw new InvalidValueException("Missing a declared authority");
                }
                $ret .= $this->authorities[$i]->toDig();
            }
        }
        if ($this->header->additionalRecordCount > 0) {
            $ret .= PHP_EOL.";; ADDITIONAL SECTION:".PHP_EOL;
            for($i = 0; $i < $this->header->additionalRecordCount; $i += 1) {
                if (!isset($this->additionalRecords[$i])) {
                    throw new InvalidValueException("Missing a declared additional record");
                }
                $ret .= $this->additionalRecords[$i]->toDig();
            }
        }
        return $ret;
    }

    /**
     * @param ResourceRecord[] $arr
     * @param int $type
     * @return ResourceRecord[]
     */
    public static function filterArray($arr, $type, $class=ResourceClass::CLASS_IN) {
        $ret = array();
        foreach($arr as $a) {
            if (!($a instanceof ResourceRecord)) {
                throw new \Exception("Tried to filter an an illigal value");
            }
            if ($a->getType() == $type && $a->getClass() == $class) {
                array_push($ret, $a);
            }
        }
        return $ret;
    }
}