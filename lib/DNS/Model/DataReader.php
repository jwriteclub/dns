<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

class DataReader {

    protected $ptr = -1;
    protected $data = "";
    protected $len = 0;

    /**
     * @param string $data the data to encapsulate
     */
    public function __construct($data) {
        $this->data = $data;
        $this->len = strlen($data);
    }

    public function hasNext($cnt=1) {
        return $this->validOffset($this->currentOffset(), $cnt);
    }

    public function next($cnt=1) {
        $ret = $this->peek($cnt);
        $this->ptr += $cnt;
        return $ret;
    }

    public function peek($cnt=1) {
        if (!$this->hasNext($cnt)) {
            throw new \Exception("Cannot read beyond end of data");
        }
        $ret = "";
        $pos = $this->ptr;
        while($cnt > 0) {
            $ret .= $this->data[++$pos];
            $cnt -= 1;
        }
        return $ret;
    }

    public function at($i, $cnt=1) {
        if (!$this->validOffset($i, $cnt)) {
            throw new \Exception("Cannot read beyond end of data");
        }
        $ret = "";
        $pos = $i;
        while($cnt > 0) {
            $ret .= $this->data[$pos++];
            $cnt -= 1;
        }
        return $ret;
    }

    public function validOffset($i, $cnt=1) {
        if ($i < 0) {
            return false;
        }
        return $i + $cnt <= $this->len;
    }

    public function rewind() {
        $this->ptr = -1;
    }

    public function currentOffset() {
        return $this->ptr + 1;
    }

}