<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

use DNS\Model\Resources\AddressResource;
use DNS\Model\Resources\CNameResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Resources\QuadAddressResource;

class ResourceRecord {

    /** @var DomainName */
    protected $name;
    protected $type;
    protected $class;
    protected $ttl;
    protected $resourceDataLength;
    protected $resourceData;

    public function __construct() {}

    public function getType() {
        return $this->type;
    }
    public function getClass() {
        return $this->class;
    }
    public function getTtl() {
        return $this->ttl;
    }
    public function getName() {
        return $this->name;
    }

    public function setTtl($ttl) {
        $this->ttl = (int)$ttl;
    }

    /**
     * @param DataReader $reader The input data
     * @return ResourceRecord
     * @throws InvalidNameException
     * @throws InvalidValueException
     */
    public static function fromBinary($reader) {
        $name = DomainName::fromBinary($reader);

        if (!$reader->hasNext(10)) {
            throw new InvalidValueException("Insufficient resource header data");
        }

        $vars = unpack("ntype/nclass/Nttl/nresourceDataLength", $reader->next(10));

        switch($vars['type']) {
            case Type::TYPE_A:
                $ret = new AddressResource();
                break;
            case Type::TYPE_NS:
                $ret = new NameserverResource();
                break;
            case Type::TYPE_CNAME:
                $ret = new CNameResource();
                break;
            case Type::TYPE_AAAA:
                $ret = new QuadAddressResource();
                break;
            default:
                $ret = new ResourceRecord();
                break;
        }
        $ret->populate($name, $vars, $reader);

        return $ret;
    }

    public function toBinary() {
        return $this->name->toBinary().pack("nnNn", $this->type, $this->class, $this->ttl, $this->resourceDataLength).$this->resourceData;
    }

    /**
     * @param DomainName $name
     * @param array $vars
     * @param DataReader $reader
     * @throws InvalidValueException
     */
    public function populate($name, $vars, $reader) {
        $this->name = $name;
        $this->type = $vars['type'];
        $this->class = $vars['class'];
        $this->ttl = $vars['ttl'];
        $this->resourceDataLength = $vars['resourceDataLength'];
        if (!$reader->hasNext($this->resourceDataLength)) {
            throw new InvalidValueException("Insufficient resource data");
        }
        $this->resourceData = $reader->next($this->resourceDataLength);
    }

    ///
    /// Formatters
    ///
    public function toDig($includeEol=true) {
        $ret = "";
        $ret .= $this->name->toString();
        $ret .= "\t\t";
        $ret .= $this->ttl;
        $ret .= "\t";
        $ret .= ResourceClass::digName($this->class);
        $ret .= "\t";
        $ret .= Type::digName($this->type);
        if ($includeEol) {
            $ret .= PHP_EOL;
        }
        return $ret;
    }
}