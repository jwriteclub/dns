<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model;

class DomainName {

    const MAX_LENGTH_LABEL = 63;
    const MAX_LENGTH_DOMAIN_NAME = 255;

    protected $labelLengths = array();
    protected $labels = array();

    protected function __construct() {}

    /**
     * Creates a domain name from a string
     * @param string $name the name to construct
     * @return DomainName
     * @throws InvalidNameException
     */
    public static function fromString($name) {
        // Create the return variable
        $ret = new DomainName();

        // Fast bailout for root name
        if ($name == ".") {
            $ret->addLabel("", true);
            return $ret;
        }

        // Determine the length of the input string
        $len = strlen($name);
        $len -= 1;

        // Check for empty domain
        if ($len < 1) {
            throw new InvalidNameException("Name is empty");
        }

        // Parse off the trailing dot, if included
        if ($name[$len] == '.') {
            $len -= 1;
        }

        // Check for overlong overall name
        if ($len + 3 >  self::MAX_LENGTH_DOMAIN_NAME) { // Why add 1? Because the binary representation has a leading dot
            throw new InvalidNameException("Name length $len is greater than the maximum permitted name ".(self::MAX_LENGTH_DOMAIN_NAME - 3));
        }

        // Push the empty label for the top level
        $ret->addLabel("", true);

        $label = "";
        do {
            // Parse through looking for labels
            $char = $name[$len];
            $chr = ord($char);
            // Got a period, try and push a label
            if ($char == '.') {
                $ret->addLabel($label);
                $label = "";
            } else if (
                // Check if this is a valid label character
                ($chr >= 48 && $chr <= 57) // Digits
            ||  ($chr >= 65 && $chr <= 90) // Capital Letters
            ||  ($chr >= 97 && $chr <= 122) // Lowercase Letters
            ||   $chr == 45
            ) {
                $label = $char.$label;
            } else {
                throw new InvalidNameException("Character code $chr is not a valid name character");
            }
            $len -= 1;
        } while ($len >= 0);

        // Add the last label
        $ret->addLabel($label);

        return $ret;
    }
    protected function addLabel($label, $allowEmpty = false) {
        $labelLen = strlen($label);
        if ($labelLen > self::MAX_LENGTH_LABEL) {
            throw new InvalidNameException("Label length $labelLen is greater than the maximum permitted label ".self::MAX_LENGTH_LABEL);
        }
        if (!$allowEmpty && $labelLen < 1) {
            throw new InvalidNameException("Label is empty");
        }
        if ($labelLen > 0) {
            if ($label[0] == '-') {
                throw new InvalidNameException("Label begins with invalid character " . $label[0]);
            }
            if ($label[$labelLen - 1] == '-') {
                throw new InvalidNameException("Label ends with invalid character -");
            }
        }
        array_unshift($this->labelLengths, $labelLen);
        array_unshift($this->labels, $label);
    }

    /**
     * Creates a typical "string" representation of this domain name
     * @param bool|true $includeRoot Whether to include the trailing dot
     * @return string
     */
    public function toString($includeRoot=true) {
        $cnt = count($this->labels);
        $ret = "";
        $sep = '';
        for($i = 0; $i < $cnt - 1; $i += 1) {
            $ret .= $sep.$this->labels[$i];
            if ($sep == '') {
                $sep = '.';
            }
        }
        if ($includeRoot) {
            $ret .= ".";
        }
        return $ret;
    }
    public function __toString() {
        return $this->toString(false);
    }

    /**
     * @param DataReader $reader
     * @return DomainName
     * @throws InvalidNameException
     */
    public static function fromBinary($reader) {
        // Create the return object
        $ret = new DomainName();

        if (!$reader->hasNext()) {
            throw new InvalidNameException("Input string is empty");
        }

        $cumulativeLength = 0;
        $ptr = $reader->currentOffset();
        $compressedMode = false;
        $followedPointers = array();
        while(true) {
            if ($cumulativeLength >= self::MAX_LENGTH_DOMAIN_NAME) {
                throw new InvalidNameException("Input string is longer than the maximum allowed length");
            }
            if (!$reader->validOffset($ptr)) {
                throw new InvalidNameException("Insufficient data");
            }
            $chr = ord($compressedMode ? $reader->at($ptr) : $reader->next());
            $cumulativeLength += 1;
            $ptr += 1;
            if ($chr >= 192) {
                if (isset($followedPointers[$ptr])) {
                    throw new InvalidNameException("Nefarious compression");
                }
                if (!$reader->validOffset($ptr)) {
                    throw new InvalidNameException("Incomplete compression");
                }
                $newPtr = (($chr - 192) << 8) + ord($compressedMode ? $reader->at($ptr) : $reader->next());
                $compressedMode = true;
                $cumulativeLength -= 1;
                $followedPointers[$ptr] = true;
                $ptr = $newPtr;
            } else {
                if ($chr > 63) {
                    throw new InvalidNameException("Input string label is too long");
                }
                if (!$reader->validOffset($ptr, $chr)) {
                    throw new InvalidNameException("Invalid label length overrun");
                }
                if ($chr == 0) {
                    array_push($ret->labels, "");
                    array_push($ret->labelLengths, 0);
                    break;
                }
                $read = $compressedMode ? $reader->at($ptr, $chr) : $reader->next($chr);
                for($i = 0; $i < $chr; $i += 1) {
                    $curr = ord($read[$i]);
                    if (!(
                        // Check if this is a valid label character
                            ($curr >= 48 && $curr <= 57) // Digits
                        ||  ($curr >= 65 && $curr <= 90) // Capital Letters
                        ||  ($curr >= 97 && $curr <= 122) // Lowercase Letters
                        ||   $curr == 45
                    )) {
                        throw new InvalidNameException("Invalid label character");
                    }
                }
                if ($read[0] == '-') {
                    throw new InvalidNameException("Label begins with invalid character " . $read[0]);
                }
                if ($read[$chr - 1] == '-') {
                    throw new InvalidNameException("Label ends with invalid character -");
                }
                array_push($ret->labels, $read);
                array_push($ret->labelLengths, $chr);
                $cumulativeLength += $chr;
                $ptr += $chr;
            }
        }
        return $ret;
/*
        if ($binary instanceof DataReader) {
            $reader = $binary;
            $binary = "";
            while ($reader->hasNext()) {
                $chr = $reader->next();
                $binary .= $chr;
                if ($chr == "\x00") {
                    break;
                }
            }
        }

        $binaryLen = strlen($binary);
        if ($binaryLen > self::MAX_LENGTH_DOMAIN_NAME) {
            throw new InvalidNameException("Input string is longer than the maximum allowed length");
        }
        if ($binaryLen < 1) {

        }

        $ptr = 0;
        while($ptr < $binaryLen) {
            $cnt = ord($binary[$ptr]);
            if ($cnt > 63) {
                throw new InvalidNameException("Input string label is too long");
            }
            $ptr += 1;
            if ($ptr + $cnt > $binaryLen) {
                throw new InvalidNameException("Invalid label length overrun");
            }
            if ($cnt == 0) { // Compatability for older php where substr on the last index returns false not empty string
                array_push($ret->labelLengths, $cnt);
                array_push($ret->labels, "");
            } else {
                array_push($ret->labelLengths, $cnt);
                array_push($ret->labels, substr($binary, $ptr, $cnt));
            }
            $ptr += $cnt;
        }

        return $ret;
*/
    }
    public function toBinary() {
        $out = "";
        $cnt = count($this->labels);
        for($i = 0; $i < $cnt; $i += 1) {
            $out .= chr($this->labelLengths[$i]);
            $out .= $this->labels[$i];
        }
        return $out;
    }

    /**
     * @param DomainName $name
     */
    public function equals(DomainName $name) {
        return $this->labelLengths === $name->labelLengths && $this->labels === $name->labels;
    }
}