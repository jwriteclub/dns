<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Model\Resources;

use DNS\Model\DomainName;
use DNS\Model\ResourceRecord;
use DNS\Model\DataReader;

class NameserverResource extends ResourceRecord {

    /**
     * @var DomainName
     */
    public $nameserverName;

    /**
     * @param DomainName $name
     * @param array $vars
     * @param DataReader $reader
     */
    public function populate($name, $vars, $reader) {
        $this->name = $name;
        $this->type = $vars['type'];
        $this->class = $vars['class'];
        $this->ttl = $vars['ttl'];
        $this->nameserverName = DomainName::fromBinary($reader);
        $this->resourceData = $this->nameserverName->toBinary();
        $this->resourceDataLength = strlen($this->resourceData);
    }

    public function toDig($includeEol=true) {
        $ret = parent::toDig(false);
        $ret .= "\t";
        $ret .= $this->nameserverName->toString();
        if ($includeEol) {
            $ret .= PHP_EOL;
        }
        return $ret;
    }
}