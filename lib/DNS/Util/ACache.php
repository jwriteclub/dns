<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Util;

use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\ResourceRecord;
use DNS\Model\Resources\AddressResource;
use DNS\Model\Type;
use DNS\Model\Resources\NameserverResource;
use DNS\Tools\BitWrapper;
use DNS\Tools\DNSRoots;

abstract class ACache {

    protected static $rootName;
    protected static $bitTld;
    protected static $instance;

    /**
     * @param DomainName $domainName
     * @param int $type One of Type::TYPE_A or Type::TYPE_AAAA
     * @return Message Null or a message pointing to an result
     */
    public function getAuthorities($domainName, $type) {
        // Glue into the DNS system
        if ($domainName->equals(self::$rootName)) {
            if ($type == Type::TYPE_A) {
                return DNSRoots::ipv4authorities();
            } else if ($type == Type::TYPE_AAAA) {
                return DNSRoots::ipv6authorities();
            } else {
                return null;
            }
        }
        $bitname = $domainName->toString();
        $bitnamelen = strlen($bitname);
        $firstdot = strpos($bitname, '.');
        if ($bitnamelen > 5 && substr($bitname, -5) == self::$bitTld && $firstdot == $bitnamelen - 5) { // Don't check if we're on a subdomain like a.b.bit
            return BitWrapper::lookup($this, $domainName, $bitname);
        }
        return null;
    }

    protected static function init() {
        if (self::$rootName == null) {
            self::$rootName = DomainName::fromString(".");
            self::$bitTld = ".bit.";
        }
    }

    /**
     * @return ACache
     */
    static function instance() {
        if (self::$instance == null) {
            self::$instance = new static();
            static::init();
        }
        return self::$instance;
    }

    /**
     * @param NameserverResource[] $authorities
     * @return void
     */
    abstract function addAuthorities($authorities);
    /**
     * @param ResourceRecord $resource
     * @return void
     */
    abstract function addResource(ResourceRecord $resource);

    /**
     * @param DomainName $name
     * @return ResourceRecord[]
     */
    function getResources(DomainName $name) {
        $ret = array();
        $roots = DNSRoots::ipv4authorities();
        foreach($roots->additionalRecords as $rec) {
            assert($rec instanceof AddressResource);
            if ($rec->getName()->equals($name)) {
                array_push($ret, $rec);
            }
        }
        return $ret;
    }

}