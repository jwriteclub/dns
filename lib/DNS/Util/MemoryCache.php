<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Util;

use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\ResourceRecord;
use DNS\Model\Resources\CNameResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Resources\AddressResource;

class MemoryCacheAuthorityEntry {
    public $expires;
    /** @var NameserverResource */
    public $resource;
}

class MemoryCacheAddressEntry {
    public $expires;
    /** @var ResourceRecord */
    public $resource;
}

class MemoryCache extends ACache{

    protected $authorities;
    protected $addresses;

    public function __construct() {
        self::init();
        $this->authorities = array();
        $this->addresses = array();
    }

    public function getAuthorities($domainName, $type) {
        $startTime = time();
        $key = $domainName->toString(true);
        //echo "MemoryCache: Checking $key".PHP_EOL;
        if (isset($this->authorities[$key])) {
            //echo "MemoryCache: Found authorities for $key".PHP_EOL;
            $ret = new Message();
            $ret->header->response = true;
            $ret->header->authoritative = true;
            foreach($this->authorities[$key] as $item) {
                assert($item instanceof MemoryCacheAuthorityEntry);
                //echo "MemoryCache: Checking item expiration ".$item->expires." vs. ".time().PHP_EOL;
                if ($item->expires >= $startTime) {
                    //echo "MemoryCache: Sending ".$item->resource->toDig();
                    array_push($ret->authorities, $item->resource);
                    $ret->header->nameserverCount += 1;
                    $addrs = $this->getResources($item->resource->nameserverName);
                    foreach($addrs as $addr) {
                        //echo "MemoryCache: Sending ".$addr->toDig();
                        array_push($ret->additionalRecords, $addr);
                        $ret->header->additionalRecordCount += 1;
                    }
                }
            }
            if (count($ret->authorities) > 0) {
                return $ret;
            }
        }
        return parent::getAuthorities($domainName, $type);
    }


    /**
     * @param NameserverResource[] $authorities
     * @return void
     */
    function addAuthorities($authorities) {
        foreach($authorities as $auth) {
            if ($auth instanceof NameserverResource) {
                $key = $auth->getName()->toString(true);
                if (!isset($this->authorities[$key])) {
                    $this->authorities[$key] = array();
                }
                $add = true;
                foreach ($this->authorities[$key] as &$existing) {
                    assert($existing instanceof MemoryCacheAuthorityEntry);
                    if ($existing->resource->nameserverName->equals($auth->nameserverName)) {
                        //echo "MemoryCache: Updating item " . $key . "/" . $auth->nameserverName . PHP_EOL;
                        $existing->expires = time() + $auth->getTtl() + 1;
                        $add = false;
                    }
                }
                if ($add) {
                    $add = new MemoryCacheAuthorityEntry();
                    $add->expires = time() + $auth->getTtl() + 1;
                    $add->resource = $auth;
                    array_push($this->authorities[$key], $add);
                }
            }
        }
    }

    /**
     * @param ResourceRecord $resource
     * @return void
     */
    function addResource(ResourceRecord $resource) {
        $key = $resource->getName()->toString(true);
        //echo "MemoryCache: Adding $key: ".$resource->toDig();
        if (!isset($this->addresses[$key])) {
            $this->addresses[$key] = array();
        }
        $add = true;
        foreach($this->addresses[$key] as &$existing) {
            assert($existing instanceof MemoryCacheAddressEntry);
            if ($resource instanceof AddressResource && $existing->resource instanceof AddressResource) {
                if ($existing->resource->ip == $resource->ip) {
                    //echo "MemoryCache: Updating A record for ".$key.PHP_EOL;
                    $existing->expires = time() + $resource->getTtl() + 1;
                    $add = false;
                }
            }
            if ($resource instanceof CNameResource && $existing->resource instanceof CNameResource) {
                if ($resource->canonicalName->equals($existing->resource->canonicalName)) {
                    //echo "MemoryCache: Updating CName record for ".$key.PHP_EOL;
                    $existing->expires = time() + $resource->getTtl() + 1;
                    $add = false;
                }
            }
        }
        if ($add) {
            $add = new MemoryCacheAddressEntry();
            $add->expires = time() + $resource->getTtl() + 1;
            $add->resource = &$resource;
            array_push($this->addresses[$key], $add);
        }
    }

    function getResources(DomainName $string) {
        $startTime = time();
        $key = $string->toString(true);
        if (!isset($this->addresses[$key])) {
            return parent::getResources($string);
        }
        $ret = array();
        foreach($this->addresses[$key] as $item) {
            assert($item instanceof MemoryCacheAddressEntry);
            if ($item->expires >= $startTime) {
                array_push($ret, $item->resource);
            }
        }
        return $ret;
    }
}