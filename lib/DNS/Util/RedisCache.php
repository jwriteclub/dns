<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Util;

use DNS\Model\DataReader;
use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\ResourceRecord;
use DNS\Model\Resources\AddressResource;
use DNS\Model\Resources\CNameResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Resources\QuadAddressResource;
use Predis\Client;

class RedisCache extends ACache {

    const ALL_AUTHORITIES = "all_authorities";
    const ALL_ADDRESSES = "all_addresses";

    /** @var Client */
    protected $redis;

    public function __construct(Client $redis) {
        $this->redis = $redis;
        static::init();
    }

    public function getAuthorities($domainName, $type) {
        $listkey = "authority-list:".$domainName->toString(true);
        $ret = null;
        if ($this->redis->sismember(self::ALL_AUTHORITIES, $listkey)) {
            $this->redis->watch(self::ALL_AUTHORITIES);
            $ret = new Message();
            $ret->header->response = true;
            $ret->header->authoritative = true;
            $this->redis->watch($listkey);
            $srem = array();
            $localkeys = $this->redis->smembers($listkey);
            //echo "RedisCache: Got local keys ".implode(", ", $localkeys).PHP_EOL;
            if (count($localkeys) > 0) {
                foreach($localkeys as $key) {
                    $val = $this->redis->get($key);
                    if ($val == null) {
                        //echo "RedisCache: Removing references to " . $key . PHP_EOL;
                        array_push($srem, $key);
                    } else {
                        $auth = NameserverResource::fromBinary(new DataReader($val));
                        $auth->setTtl($this->redis->ttl($key));
                        array_push($ret->authorities, $auth);
                        $ret->header->nameserverCount += 1;
                    }
                }
            }
            if (count($srem) > 0) {
                //echo "RedisCache: Removing " . implode(", ", $srem);
                $this->redis->multi();
                if (count($localkeys) == count($srem)) {
                    $this->redis->del($listkey);
                    $this->redis->srem(self::ALL_AUTHORITIES, $listkey);
                    $ret = null;
                } else {
                    $this->redis->srem($listkey, $srem);
                }
                $this->redis->exec();
            } else {
                $this->redis->unwatch();
            }
        }
        if ($ret != null) {
            return $ret;
        }
        return parent::getAuthorities($domainName, $type);
    }

    /**
     * @param NameserverResource[] $authorities
     * @return void
     */
    function addAuthorities($authorities) {
        foreach($authorities as $auth) {
            //echo "RedisCache: Add authority ".$auth->toDig();
            if (!($auth instanceof NameserverResource)) {
                continue; // Ignore SOA for now
            }
            $listkey = "authority-list:".$auth->getName()->toString(true);
            $localkey = "authority:".$auth->getName()->toString(true).":".$auth->nameserverName->toString(true);
            $this->redis->transaction()
                ->sadd(self::ALL_AUTHORITIES, $listkey)
                ->sadd($listkey, $localkey)
                ->set($localkey, $auth->toBinary())
                ->expire($localkey, $auth->getTtl())
                ->execute();
        }
    }

    /**
     * @param ResourceRecord $resource
     * @return void
     */
    function addResource(ResourceRecord $resource) {
        //echo "RedisCache: Add ".$resource->toDig();
        $listkey = "address-list:".$resource->getName()->toString(true);
        if ($resource instanceof AddressResource || $resource instanceof QuadAddressResource) {
            $localkey = "address:" . $resource->getName()->toString(true) . ":" . inet_ntop($resource->ip);
        } else if ($resource instanceof CNameResource) {
            $localkey = "address:" . $resource->getName()->toString(true) . ":" . $resource->canonicalName->toString(true);
        } else {
            $localkey = "address:" . $resource->getName()->toString(true) . ":" . get_class($resource);
        }
        $this->redis->transaction()
            ->sadd(self::ALL_ADDRESSES, $listkey)
            ->sadd($listkey, $localkey)
            ->set($localkey, $resource->toBinary())
            ->expire($localkey, $resource->getTtl())
            ->execute();
    }

    public function getResources(DomainName $domainName) {
        $listkey = "address-list:".$domainName->toString(true);
        $ret = null;
        if ($this->redis->sismember(self::ALL_ADDRESSES, $listkey)) {
            $this->redis->watch(self::ALL_ADDRESSES);
            $ret = array();
            $this->redis->watch($listkey);
            $srem = array();
            $localkeys = $this->redis->smembers($listkey);
            //echo "RedisCache: Got local keys ".implode(", ", $localkeys).PHP_EOL;
            if (count($localkeys) > 0) {
                foreach($localkeys as $key) {
                    $val = $this->redis->get($key);
                    if ($val == null) {
                        //echo "RedisCache: Removing references to " . $key . PHP_EOL;
                        array_push($srem, $key);
                    } else {
                        $auth = AddressResource::fromBinary(new DataReader($val));
                        $auth->setTtl($this->redis->ttl($key));
                        array_push($ret, $auth);
                    }
                }
            }
            if (count($srem) > 0) {
                //echo "RedisCache: Removing " . implode(", ", $srem).PHP_EOL;
                $this->redis->multi();
                if (count($localkeys) == count($srem)) {
                    $this->redis->del($listkey);
                    $this->redis->srem(self::ALL_ADDRESSES, $listkey);
                    $ret = null;
                } else {
                    $this->redis->srem($listkey, $srem);
                }
                $this->redis->exec();
            } else {
                $this->redis->unwatch();
            }
        }
        if ($ret != null) {
            return $ret;
        }
        return parent::getResources($domainName);
    }
}