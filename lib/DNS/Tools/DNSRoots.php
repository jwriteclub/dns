<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Tools;

use DNS\Model\DataReader;
use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\ResourceClass;
use DNS\Model\Resources\AddressResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Resources\QuadAddressResource;
use DNS\Model\Type;

final class DNSRoots {

    const ROOT4_A = "198.41.0.4";
    const ROOT4_B = "192.228.79.201";
    const ROOT4_C = "192.33.4.12";
    const ROOT4_D = "199.7.91.13";
    const ROOT4_E = "192.203.230.10";
    const ROOT4_F = "192.5.5.241";
    const ROOT4_G = "192.112.36.4";
    const ROOT4_H = "198.97.190.53";
    const ROOT4_I = "192.36.148.17";
    const ROOT4_J = "192.58.128.30";
    const ROOT4_K = "193.0.14.129";
    const ROOT4_L = "199.7.83.42";
    const ROOT4_M = "202.12.27.33";

    const ROOT6_A = "2001:503:BA3E::2:30";
    const ROOT6_B = "2001:500:84::b";
    const ROOT6_C = "2001:500:2::c";
    const ROOT6_D = "2001:500:2D::d";
    //const ROOT6_E = ""; Ames is not v6 supported, yet
    const ROOT6_F = "2001:500:2f::f";
    //const ROOT6_G = "";
    const ROOT6_H = "2001:500:1::53";
    const ROOT6_I = "2001:7fe::53";
    const ROOT6_J = "2001:503:C27::2:30";
    const ROOT6_K = "2001:7fd::1";
    const ROOT6_L = "2001:500:9f::42";
    const ROOT6_M = "2001:dc3::35";

    protected static $v4roots = array(
        "a.root-servers.net." => self::ROOT4_A,
        "b.root-servers.net." => self::ROOT4_B,
        "c.root-servers.net." => self::ROOT4_C,
        "d.root-servers.net." => self::ROOT4_D,
        "e.root-servers.net." => self::ROOT4_E,
        "f.root-servers.net." => self::ROOT4_F,
        "g.root-servers.net." => self::ROOT4_G,
        "h.root-servers.net." => self::ROOT4_H,
        "i.root-servers.net." => self::ROOT4_I,
        "j.root-servers.net." => self::ROOT4_J,
        "k.root-servers.net." => self::ROOT4_K,
        "l.root-servers.net." => self::ROOT4_L,
        "m.root-servers.net." => self::ROOT4_M
    );

    protected static $v6roots = array(
        "a.root-servers.net." => self::ROOT6_A,
        "b.root-servers.net." => self::ROOT6_B,
        "c.root-servers.net." => self::ROOT6_C,
        "d.root-servers.net." => self::ROOT6_D,
        //"e.root-servers.net." => self::ROOT6_E,
        "f.root-servers.net." => self::ROOT6_F,
        //"g.root-servers.net." => self::ROOT6_G,
        "h.root-servers.net." => self::ROOT6_H,
        "i.root-servers.net." => self::ROOT6_I,
        "j.root-servers.net." => self::ROOT6_J,
        "k.root-servers.net." => self::ROOT6_K,
        "l.root-servers.net." => self::ROOT6_L,
        "m.root-servers.net." => self::ROOT6_M
    );

    /** @var Message */
    protected static $v4response;
    /** @var Message */
    protected static $v6response;

    /**
     * @return Message
     * @throws \DNS\Model\InvalidNameException
     * @throws \DNS\Model\InvalidValueException
     */
    public static function ipv4authorities() {
        if (self::$v4response == null) {
            $rootName = DomainName::fromString(".");

            $m = new Message();
            $m->header->response = true;
            $m->header->authoritative = true;

            foreach (self::$v4roots as $namestring => $ipstring) {
                $ns = new NameserverResource();
                $nsname = DomainName::fromString($namestring);
                $bstr = $nsname->toBinary();
                $ns->populate($rootName, array("type" => Type::TYPE_NS, "class" => ResourceClass::CLASS_IN, "ttl" => 0, "resourceDataLength" => strlen($bstr)), new DataReader($bstr));

                $a = new AddressResource();
                $a->populate($nsname, array("type" => Type::TYPE_A, "class" => ResourceClass::CLASS_IN, "ttl" => 0, "resourceDataLength" => 4), new DataReader(inet_pton($ipstring)));

                array_push($m->authorities, $ns);
                array_push($m->additionalRecords, $a);

                $m->header->nameserverCount += 1;
                $m->header->additionalRecordCount += 1;
                self::$v4response = $m;
            }
        }
        shuffle(self::$v4response->authorities);
        return self::$v4response;
    }

    /**
     * @return Message
     * @throws \DNS\Model\InvalidNameException
     * @throws \DNS\Model\InvalidValueException
     */
    public static function ipv6authorities() {
        if (self::$v6response == null) {
            $rootName = DomainName::fromString(".");

            $m = new Message();
            $m->header->response = true;
            $m->header->authoritative = true;

            foreach (self::$v6roots as $namestring => $ipstring) {
                $ns = new NameserverResource();
                $nsname = DomainName::fromString($namestring);
                $bstr = $nsname->toBinary();
                $ns->populate($rootName, array("type" => Type::TYPE_NS, "class" => ResourceClass::CLASS_IN, "ttl" => 0, "resourceDataLength" => strlen($bstr)), new DataReader($bstr));

                $a = new QuadAddressResource();
                $a->populate($nsname, array("type" => Type::TYPE_AAAA, "class" => ResourceClass::CLASS_IN, "ttl" => 0, "resourceDataLength" => 16), new DataReader(inet_pton($ipstring)));

                array_push($m->authorities, $ns);
                array_push($m->additionalRecords, $a);

                $m->header->nameserverCount += 1;
                $m->header->additionalRecordCount += 1;
                self::$v6response = $m;
            }
        }
        shuffle(self::$v6response->authorities);
        return self::$v6response;
    }
}