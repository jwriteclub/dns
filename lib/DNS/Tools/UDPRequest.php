<?php

/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Tools;

use DNS\Model\DataReader;
use DNS\Model\Message;

class UDPRequest extends ARequest {

    const MAX_UDP_READ = 516;
    const MAX_MS = 1000;
    const SLEEP_INTERVAL_MS = 1;

    /**
     * @param string $server IP to query
     * @return Message|null
     */
    function run($server, $port=53) {
        $this->started = true;

        $packet = $this->prepareMessage()->toBinary();
        $packet_length = strlen($packet);

        $socket = @socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        if ($socket === false) {
            $err = socket_last_error();
            $this->error = socket_strerror($err);
        } else {
            $sent = @socket_sendto($socket, $packet, $packet_length, MSG_EOR, $server, $port);
            $ret = null;
            if ($sent != $packet_length) {
                $this->error = "Unable to send to socket";
            } else {

                $read = "";
                $port_from = null;
                $time = 0;
                do {
                    if ($time > self::MAX_MS) {
                        break;
                    }
                    if ($read >= self::MAX_UDP_READ) {
                        break;
                    }
                    // TODO: Receive timeout
                    socket_recvfrom($socket, $buffer, self::MAX_UDP_READ, MSG_DONTWAIT, $server, $port_from);
                    $read .= $buffer;
                    if (strlen($buffer) == 0) {
                        time_nanosleep(0, self::SLEEP_INTERVAL_MS * 1000000);
                        $time += self::SLEEP_INTERVAL_MS;
                        //echo "At time $time, sleeping".PHP_EOL;
                    }
                    try {
                        $ret = Message::fromBinary(new DataReader($read));
                        if ($ret != null) {
                            break;
                        }
                    } catch (\Exception $ex) {
                    };
                    $buffer = "";
                } while (true);

                $this->received = strlen($read);
                //echo "Got $this->received in ${time}ms".PHP_EOL;

                if ($ret == null) {
                    try {
                        $ret = Message::fromBinary(new DataReader($read));
                    } catch (\Exception $ex) {
                        //echo $ex->getMessage();
                        $this->error = $ex;
                        $ret = null;
                    }
                }
            }
            @socket_close($socket);
        }

        $this->finished = true;

        return $ret;
    }
}