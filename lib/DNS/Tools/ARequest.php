<?php

/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Tools;

use DNS\Model\Message;
use DNS\Model\Question;

abstract class ARequest {

    /** @var Question[] */
    protected $questions = array();
    protected $started = false;
    protected $finished = false;
    protected $error = false;
    protected $received = 0;
    protected $shouldRecurse = true;

    /**
     * @param Question $question
     * @throws IlligalStateException
     */
    public function addQuestion($question) {
        if ($this->started) {
            throw new IlligalStateException("Trying to add a question to a started request");
        }
        array_push($this->questions, $question);
    }

    /**
     * @return Message
     */
    protected function prepareMessage() {
        $this->started = true;
        $message = new Message();
        foreach($this->questions as $q) {
            $message->addQuestion($q);
        }
        $message->setRequest($this->shouldRecurse);
        return $message;
    }
    public function setShouldRecurse($shouldRecurse) {
        $this->shouldRecurse = $shouldRecurse;
    }

    public function getReceivedLength() {
        return $this->received;
    }

    abstract function run($server, $port=53);

}