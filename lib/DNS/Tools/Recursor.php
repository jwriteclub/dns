<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Tools;

use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\QueryType;
use DNS\Model\QueryClass;
use DNS\Model\Question;
use DNS\Model\Resources\AddressResource;
use DNS\Model\Resources\CNameResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Type;
use DNS\Util\ACache;
use DNS\Util\MemoryCache;

class Recursor {

    public function __construct(ACache $cache) {
        $this->cache = $cache;
    }

    /** @var Question[] */
    protected $questions = array();
    protected $started = false;
    protected $received = 0;
    protected $cache = null;

    /**
     * @param Question $question
     */
    public function addQuestion($question) {
        if ($this->started) {
            throw new IlligalStateException("Tried to add a question to a started recursor");
        }
        array_push($this->questions, $question);
    }

    /**
     * @param bool $print
     * @return Message
     * @throws IlligalStateException
     * @throws \DNS\Model\InvalidValueException
     * @throws \Exception
     */
    public function run($server = null, $forceRecursion = false, $print = false, $recursionTarget = array()) {
        $this->started = true;
        $answer = null;
        $servers = null;

        $iterations = 0;
        $lastlen = -1;

        do {
            if ($servers == null) {
                $parts = explode(".", $this->questions[0]->name->toString("."));
                $len = count($parts);
                while ($answer == null && count($parts) > 0) {
                    $curr = implode(".", $parts);
                    if ($curr == "") {
                        $curr = ".";
                    }
                    $curr = DomainName::fromString($curr);
                    //echo"Looking for authorities with " . $curr->toString(true) . PHP_EOL;

                    $answer = $this->cache->getAuthorities($curr, Type::TYPE_A);
                    array_shift($parts);
                    $len -= 1;
                }
                if ($answer == null || !($answer instanceof Message) || count($answer->authorities) < 1) {
                    throw new \Exception("Cannot find authorities for any option");
                }
                if ($len == $lastlen) {
                    throw new \Exception("Tried all peers at level $len and nobody has an answer. Faililng");
                }
                $lastlen = $len;
                if ($print) {
                    //echo$answer->toDig() . PHP_EOL;
                }
                $servers = array();
                foreach ($answer->authorities as $authority) {
                    if (!($authority instanceof NameserverResource)) {
                        throw new \Exception("Server responded with a non-NS");
                    }
                    $authority = $authority->nameserverName;
                    array_push($servers, $authority);
                }
                //echo"Working with servers ".implode(" ", $servers).PHP_EOL;
            }
            foreach($servers as $server) {
                //echo"Trying to contact server $server ..";
                if ($server == "local.internal") {
                    //echo". doing local lookup ..";
                    $m = new Message();
                    $m->header->authoritative = true;
                    $m->header->response = true;
                    foreach($this->questions as $q) {
                        $recs = $this->cache->getResources($q->name);
                        foreach($recs as $r) {
                            if ($r != null) {
                                array_push($m->answers, $r);
                                $m->header->answerCount += 1;
                            }
                        }
                        array_push($m->questions, $q);
                        $m->header->questionCount += 1;
                    }
                    $answer = $m;
                    //echo". Done".PHP_EOL;
                    break 2;
                } else {
                    $resources = $this->cache->getResources($server);
                    $ips = array();
                    foreach($resources as $res) {
                        if ($res instanceof AddressResource) {
                            array_push($ips, $res);
                        }
                    }
                    if (count($ips) < 1) {
                        foreach($recursionTarget as $rServer) {
                            //echo". Checking recursion target " . $rServer;
                            if ($server->equals($rServer)) {
                                //echo". Recursion loop detected, bailing" . PHP_EOL;
                                return null;
                            }
                        }
                        //echo". not found in cache, recursing ..";
                        $r = new Recursor($this->cache);
                        $q = new Question($server);
                        $q->type = QueryType::TYPE_A;
                        $q->class = QueryClass::CLASS_IN;
                        $r->addQuestion($q);
                        try {
                            array_push($recursionTarget, $server);
                            $intermediate = $r->run(null, $forceRecursion, $print, $recursionTarget);
                        } catch (\Exception $ex) {
                            $intermediate = null;
                            //echo$ex->getMessage() . PHP_EOL;
                        }
                        if ($intermediate == null) {
                            //echo". Failed" . PHP_EOL;
                            continue;
                        }
                        foreach ($intermediate->answers as $ipans) {
                            if ($ipans instanceof AddressResource) {
                                array_push($ips, $ipans);
                            }
                        }
                    }
                    //echo". found " . count($ips) . " ips" . PHP_EOL;
                    foreach ($ips as $ip) {
                        $u = new UDPRequest();
                        foreach ($this->questions as $q) {
                            $u->addQuestion($q);
                        }
                        $u->setShouldRecurse(false);
                        $actualip = inet_ntop($ip->ip);
                        //echo"Running with $actualip" . PHP_EOL;
                        try {
                            $answer = $u->run($actualip);
                        } catch (\Exception $ex) {
                            //echo"Failure with ip $actualip" . PHP_EOL;
                            continue;
                        }
                        if ($answer == null) {
                            //echo"Got no answer with ip $actualip" . PHP_EOL;
                            continue;
                        }
                        $success = false;
                        //echo"DEBUG---".PHP_EOL.$answer->toDig()."---DEBUG".PHP_EOL;
                        if ($answer->header->nameserverCount > 0) {
                            $this->cache->addAuthorities($answer->authorities);
                            $success = true;
                        }
                        if ($answer->header->additionalRecordCount > 0) {
                            foreach ($answer->additionalRecords as $addr) {
                                $this->cache->addResource($addr);
                            }
                            $success = true;
                        }
                        if ($print) {
                            //echo$answer->toDig() . PHP_EOL;
                        }
                        if (count($answer->answers) > 0) {
                            foreach($answer->answers as $ans) {
                                $this->cache->addResource($ans);
                            }
                            $success = true;
                        }
                        if ($success) {
                            break 2;
                        }
                    }
                }
            }
            if ($answer == null) {
                throw new \Exception("All nameservers failed");
            }
            if (count($answer->answers) < 1) {
                $answer = null;
                $servers = null;
            }
            $iterations += 1;
        } while($answer == null && $iterations < 10);

        if ($answer == null) {
            return null;
        } else {
            return $answer;
        }
    }
    public function getReceivedLength() {
        return $this->received;
    }

    /**
     * @param string|DomainName $string The domain name (string form) to look up
     */
    public static function getIps($string, $cache = null, $forceRecursion = false, $includeCnames = false) {
        if ($cache == null) {
            $cache = MemoryCache::instance();
        }
        $intermediates = array();
        $questions = array($string);
        $ret = array();
        $next = array();
        $iters = 0;
        while(count($questions) > 0) {
            //echoPHP_EOL." === ITERATION $iters ===".PHP_EOL.implode(", ", $questions).PHP_EOL.PHP_EOL;
            foreach($questions as $quest) {
                if ((!$quest instanceof DomainName)) {
                    $quest = DomainName::fromString($quest);
                }
                if (!$forceRecursion && ($cached = $cache->getResources($quest)) && count($cached) > 0) {
                    foreach($cached as $answer) {
                        if ($answer instanceof AddressResource) {
                            array_push($ret, $answer);
                        } else if ($answer instanceof CNameResource) {
                            array_push($next, $answer);
                            array_push($intermediates, $answer);
                        }
                    }
                } else {
                    $q = new Question($quest);
                    $q->type = \DNS\Model\QueryType::TYPE_A;
                    $q->class = \DNS\Model\QueryClass::CLASS_IN;

                    $recursor = new \DNS\Tools\Recursor($cache);
                    $recursor->addQuestion($q);

                    $r = $recursor->run(null, $forceRecursion, false);
                    if ($r == null) {
                        return null;
                    }
                    foreach ($r->answers as $answer) {
                        if ($answer instanceof AddressResource) {
                            array_push($ret, $answer);
                        } else if ($answer instanceof CNameResource) {
                            array_push($next, $answer);
                            array_push($intermediates, $answer);
                        }
                    }
                }
            }
            $questions = array();
            if (count($next) > 0) {
                foreach($next as $cname) {
                    assert($cname instanceof CNameResource);
                    array_push($questions, $cname->canonicalName);
                }
                $next = array();
            }
            $iters += 1;
            if ($iters >= 10) {
                break;
            }
        }
        $m = new Message();
        $m->header->authoritative = false;
        $m->header->response = true;
        if ($includeCnames) {
            foreach ($intermediates as $rec) {
                array_push($m->answers, $rec);
                $m->header->answerCount += 1;
            }
        }
        foreach($ret as $rec) {
            array_push($m->answers, $rec);
            $m->header->answerCount += 1;
        }
        return $m;
    }
}