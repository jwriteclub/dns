<?php
/*
 * This file is part of the dns project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

namespace DNS\Tools;

use DNS\Model\DataReader;
use DNS\Model\DomainName;
use DNS\Model\Message;
use DNS\Model\ResourceClass;
use DNS\Model\Resources\AddressResource;
use DNS\Model\Resources\NameserverResource;
use DNS\Model\Type;
use DNS\Util\ACache;
use GuzzleHttp\Client;

class BitWrapper {
    /**
     * @param ACache $cache
     * @param DomainName $domainName
     * @param string $name
     * @return null|Message
     * @throws \DNS\Model\InvalidNameException
     */
    public static function lookup(ACache $cache, $domainName, $name) {
        $guzzle = new Client();
        $res = $guzzle->get("https://nmc.tenta.io/api/v1/lookup/$name");
        if ($res->getStatusCode() != 200) {
            return null;
        }
        $res = json_decode($res->getBody()->getContents());
        if (!isset($res->code) || $res->code != 200) {
            return null;
        }
        $data = $res->data;
        if ($data->ns != null) {
            $msg = new Message();
            $msg->header->response = true;
            $msg->header->authoritative = true;
            foreach($data->ns as $server) {
                $ns = new NameserverResource();
                $nsname = DomainName::fromString($server);
                $bstr = $nsname->toBinary();
                $ns->populate($domainName, array("type" => Type::TYPE_NS, "class" => ResourceClass::CLASS_IN, "ttl" => $res->data->ttl, "resourceDataLength" => strlen($bstr)), new DataReader($bstr));
                array_push($msg->authorities, $ns);
                $msg->header->nameserverCount += 1;
            }
            return $msg;
        }
        if($data->ip != null) {
            foreach($data->ip as $ip) {
                $addr = new AddressResource();
                echo PHP_EOL.$ip.PHP_EOL;
                $addr->populate($domainName, array("type" => Type::TYPE_A, "class" => ResourceClass::CLASS_IN, "ttl" => $res->data->ttl, "resourceDataLength" => 4), new DataReader(inet_pton($ip)));
                $cache->addResource($addr);
            }
            $msg = new Message();
            $msg->header->response = true;
            $msg->header->authoritative = true;
            $ns = new NameserverResource();
            $nsname = DomainName::fromString("local.internal");
            $bstr = $nsname->toBinary();
            $ns->populate($domainName, array("type" => Type::TYPE_NS, "class" => ResourceClass::CLASS_IN, "ttl" => $res->data->ttl, "resourceDataLength" => strlen($bstr)), new DataReader($bstr));
            array_push($msg->authorities, $ns);
            $msg->header->nameserverCount += 1;
            return $msg;
        }
        return null;
    }
}