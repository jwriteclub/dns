<?php
/*
 * This file is part of the dns library project, licensed under
 * the MIT open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/dns
 *
 * Copyright (c) 2016 Tenta, LLC
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@tenta.io
 *
 */

if ($argc != 2) {
    echo "Usage: php example/query.php NAME".PHP_EOL;
    exit(1);
}

date_default_timezone_set('GMT');

// 2.i Set up the autoloader
require_once(dirname(__FILE__)."/../vendor/autoload.php");

// Start time
$starttime = microtime(true);

$r = \DNS\Tools\Recursor::getIps($argv[1], null, false, true);

$ms = intval((microtime(true) - $starttime)*1000);

echo $r->toDig().PHP_EOL;
echo ";; Query time: $ms ms".PHP_EOL;
//echo ";; SERVER: $host#$port_to".PHP_EOL;
echo ";; WHEN: ".date("D M j H:i:s Y", intval($starttime)).PHP_EOL;
//echo ";; MSG SIZE  rcvd: ".$recursor->getReceivedLength().PHP_EOL.PHP_EOL;
